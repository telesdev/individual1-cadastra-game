const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    email : {
        type : String,
        required : true,
        unique : true
    },
    senha : {
        type : String,
        required : true
    },
    nome : {
        type : String,
        required : true
    },
    data: {
        type : Date,
        default : Date.now
    }
})   

module.exports = mongoose.model('users', UserSchema)