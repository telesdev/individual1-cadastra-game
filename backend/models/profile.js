const mongoose = require('mongoose')

const ProfileSchema = new mongoose.Schema({
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'user'
    },
    jogos : [{
        nome : {
            type : String,
        },
        personagemPrincipal : {
            type : String,
        },
        plataforma : {
            type : String,
        }
    }],
    cidade : {
        type : String,
    },
    bairro : {
        type : String,
    },
    discord : {
        type : String
    },
    telefone : {
        type : String
    },
    data: {
        type : Date,
        default : Date.now
    }
})   

module.exports = mongoose.model('profiles', ProfileSchema)