const express = require('express')
var bodyParser = require('body-parser')
const connectDB = require('./config/db')

var cors = require('cors')
const path = require('path')
const app = express()
const PORT = process.env.PORT || 3001


// Init Middleware
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(cors())


// Connect Database
connectDB()

// Define Routes
app.use('/', require('./routes/hello'))
app.use('/user', require('./routes/api/user'))
app.use('/profile', require('./routes/api/profile'))
app.use('/auth', require('./routes/api/auth'))

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname))
})

app.listen(PORT, () => {console.log(`App rodando na porta ${PORT}`)})