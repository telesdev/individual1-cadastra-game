const express = require('express')
const User = require('../../models/user')
const { check, validationResult } = require('express-validator');
const router = express.Router()
const bcrypt = require('bcryptjs');
const auth = require('../../middleware/auth')


// @route    GET /user/
// @desc     LIST users
// @access   Private
router.get('/', auth, async (req, res, next) => {
    try{
        const user = await User.find({}).select('-senha')
        res.json(user)
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

// @route    GET /user/:userId
// @desc     DETAIL user
// @access   Public
router.get('/:userId', [], async (req, res, next)=> {
    try {
        let paramId = req.params["userId"]
        const user = await User.findOne({ _id : paramId })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({"error" : "User not exist"})
        }
    } catch(err){
      console.error(err.message)
      res.status(500).send({"error" : "Server Error"})
    }
})

// @route    POST /user
// @desc     CREATE user
// @access   Public
router.post('/', [
    check('email', 'email não é válido').isEmail(),
    check('nome', "nome não é válido").isLength({min : 3}).isAlpha(['pt-BR']),
    check('senha', 'a senha deverá conter 6 ou mais caracteres').isLength({min:6})
], async (req, res, next) => {
    try{
        let { email, senha, nome } = req.body
        let existente = await User.findOne({email:email}) 
        if (existente) {
            res.status(400).send({"error" : "Email já existe"})
        }
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            res.status(400).json({ errors: errors.array() })
        }
        let usuario = new User({ email, senha, nome })      
        const salt = await bcrypt.genSalt(10)
        usuario.senha = await bcrypt.hash(senha, salt)
        await usuario.save()
        if (usuario.id) {
            res.status(201).json(usuario)
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

// @route    PUT /user/:userId
// @desc     EDIT user
// @access   Public
router.put('/:userId', [
    check('email', 'email não é válido').isEmail(),
    check('nome', "nome não é válido").isLength({min : 3}).isAlpha(['pt-BR']),
    check('senha', "senha em branco").exists({checkFalsy : true})
], async (req, res, next) => {
    try { 
        let paramId = req.params["userId"]
        let { email, senha, nome } = req.body
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            return res.status(400).json({ errors: errors.array() })
        }

        let update =  { email, senha, nome }

        const salt = await bcrypt.genSalt(10)
        update.senha = await bcrypt.hash(senha, salt)

        let user = await User.findOneAndReplace({_id : paramId}, update, {new : true})
        if(user) {
            res.status(202).send(user)
        } else {
            res.status(404).send({"error" : "User not exist"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

// @route    PATCH /user/:userId
// @desc     PARTIAL EDIT user
// @access   Public
router.patch('/:userId', [], async (req, res, next)=> {
    try { 
        let paramId = req.params["userId"]
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            res.status(400).json({ errors: errors.array() })
        }

        var update = req.body

        for (const [chave, valor] of Object.entries(update)){
            if (!valor){
                delete update[chave]
            }
        }

        const salt = await bcrypt.genSalt(10)
        if (req.body.senha){
            req.body.senha = await bcrypt.hash(req.body.senha, salt)
        }

        let user = await User.findOneAndUpdate({_id:paramId}, {$set:update}, {new:true})
        if(user) {
            res.status(202).json(user)
        } else {
            res.status(404).send({"error" : "User not exist"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

// @route    DELETE /user/:userId
// @desc     DELETE user
// @access   Public
router.delete('/:userId', async (req, res, next)=> {
    try{
        let paramId = req.params["userId"]
        const user = await User.findOneAndDelete({ _id : paramId })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({"error" : "User not exist"})
        }
    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})


module.exports = router