const express = require('express')
const Profile = require('../../models/profile')
const User = require('../../models/user')
const router = express.Router()
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth')


// @route    GET /profile/
// @desc     LIST profile
// @access   Private
router.get('/', auth, async (req, res, next)=> {
    try {
        console.log(req.query)
        const profiles = await Profile.find(req.query)
        res.json(profiles)
    } catch(err){
      console.error(err.message)
      res.status(500).send({"error" : "Server Error"})
    }
})

// @route    GET /profile/:userId
// @desc     DETAIL profile
// @access   Private
router.get('/:userId', auth, async (req, res, next)=> {
    try {
        const paramId = req.params["userId"]
        const profile = await Profile.findOne({ user : paramId })
        if(profile){
            res.json(profile)
        } else {
            res.status(404).send({"error" : "Profile not exist"})
        }
    } catch(err){
      console.error(err.message)
      res.status(500).send({"error" : "Server Error"})
    }
})

// @route    POST /profile
// @desc     CREATE profile
// @access   Private
router.post('/', [], async (req, res, next) => {
    try {
        let { user, jogos, cidade, bairro, discord, telefone } = req.body
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            return res.status(400).json({ errors : errors.array() })
        } else {
            const user_model = await User.findOne({ _id : user})
            if (user_model) {
                let profile = new Profile({ user, jogos, cidade, bairro, discord, telefone })
                await profile.save()
                if (profile.id) {
                    res.json(profile)
                }
            } else {
                res.status(404).send({ "error" : "User not found"})
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error" : "Server Error"})
    }
})

module.exports = router